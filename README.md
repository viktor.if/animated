# Animated

Presentation of the React Native animation system

[![TypeScript](https://img.shields.io/badge/%3C%2F%3E-TypeScript-%230074c1.svg)](http://www.typescriptlang.org/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## Development environment

- React Native (0.65.1)
- React (17.0.2)

## Issues

- React Navigation. Navigate method. Seems it works incorrectly with types
