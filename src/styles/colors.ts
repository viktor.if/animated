export const black = '#000000';
export const white = '#FFFFFF';
export const transparent = 'rgba(0,0,0,0)';

// Greyish shades
export const greyish1 = 'rgba(249,249,249,0.94)';
export const greyish2 = 'rgba(0,0,0,0.3)';
export const greyish3 = '#C6C6C8';
