import { StyleSheet } from 'react-native';
import { Colors } from '../../styles';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 160,
    height: 160,
    backgroundColor: 'tomato',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: Colors.white,
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontWeight: 'bold',
    marginVertical: 8,
    textTransform: 'uppercase',
  },
});
