import React, { useMemo } from 'react';
import { View, Animated, Text, TouchableWithoutFeedback, StyleSheet } from 'react-native';

import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const opacity = useMemo(() => new Animated.Value(1), []);

  const handlePress = () => {
    animation.setValue(0);
    opacity.setValue(1);
    Animated.timing(animation, { toValue: 1, duration: 1500, useNativeDriver: false }).start(
      ({ finished }) => {
        if (finished) {
          Animated.timing(opacity, { toValue: 0, duration: 200, useNativeDriver: false }).start();
        }
      },
    );
  };

  const progressInterpolated = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['0%', '100%'],
    extrapolate: 'clamp',
  });

  const colorInterpolated = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['rgb(71,255,99)', 'rgb(99,71,255)'],
    extrapolate: 'clamp',
  });

  const progressStyle = {
    width: progressInterpolated,
    bottom: 0,
    backgroundColor: colorInterpolated,
    opacity,
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={handlePress}>
        <View style={styles.button}>
          <View style={StyleSheet.absoluteFill}>
            <Animated.View style={[styles.progress, progressStyle]} />
          </View>
          <Text style={styles.buttonText}>Get it!</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};
