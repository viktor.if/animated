import React, { useMemo, useState, useRef } from 'react';
import { View, Animated, Text, TextInput, TouchableOpacity } from 'react-native';

import styles from './styles';

export default () => {
  const notificationRef = useRef<View>(null);
  const [value, setValue] = useState<string>('');
  const [notification, setNotification] = useState<string>('');
  const opacity = useMemo(() => new Animated.Value(0), []);
  const offset = useMemo(() => new Animated.Value(0), []);

  const hanldePress = () => {
    setNotification(value);
    setValue('');
    notificationRef.current?.measure((x, y, width, height) => {
      offset.setValue(height * -1);

      Animated.sequence([
        Animated.parallel([
          Animated.timing(opacity, { toValue: 1, duration: 300, useNativeDriver: false }),
          Animated.timing(offset, { toValue: 0, duration: 300, useNativeDriver: false }),
        ]),
        Animated.delay(1500),
        Animated.parallel([
          Animated.timing(opacity, { toValue: 0, duration: 300, useNativeDriver: false }),
          Animated.timing(offset, { toValue: height * -1, duration: 300, useNativeDriver: false }),
        ]),
      ]).start();
    });
  };

  const notificationStyle = { opacity };

  return (
    <View style={styles.screen}>
      <Animated.View style={[styles.notification, notificationStyle]} ref={notificationRef}>
        <Text style={styles.notificationText}>{notification}</Text>
      </Animated.View>
      <View>
        <TextInput
          value={value}
          onChangeText={(text) => setValue(text)}
          style={styles.input}
          placeholder="Type something here"
          placeholderTextColor="#ccc"
        />
        <TouchableOpacity onPress={hanldePress}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Show Notification</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};
