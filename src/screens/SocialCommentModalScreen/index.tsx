import React, { useMemo, useRef } from 'react';
import { View, Animated, Text, TextInput, ScrollView, PanResponder } from 'react-native';

import styles from './styles';

export default () => {
  const animated = useMemo(() => new Animated.Value(0), []);
  const animatedMargin = useMemo(() => new Animated.Value(0), []);
  let scrollOffset = useRef<number>(0);
  let contentHeight = useRef<number>(0);
  const scrollViewHeight = useRef<number>(0);

  const panResponder = useMemo(
    () =>
      PanResponder.create({
        onMoveShouldSetPanResponder: (_, gestureState) => {
          const { dy } = gestureState;
          const totalScrollHeight = scrollOffset.current + scrollViewHeight.current;

          if (
            (scrollOffset.current <= 0 && dy > 0) ||
            (totalScrollHeight >= contentHeight.current && dy < 0)
          ) {
            return true;
          }

          return false;
        },
        onPanResponderMove: (__, gestureState) => {
          const { dy } = gestureState;

          if (dy < 0) {
            animated.setValue(dy);
          } else if (dy > 0) {
            animatedMargin.setValue(dy);
          }
        },
        onPanResponderRelease: (__, gestureState) => {
          const { dy } = gestureState;

          if (dy < -150) {
            Animated.parallel([
              Animated.timing(animated, {
                toValue: -400,
                duration: 150,
                useNativeDriver: false,
              }),
              Animated.timing(animatedMargin, {
                toValue: 0,
                duration: 150,
                useNativeDriver: false,
              }),
            ]).start();
          } else if (dy > -150 && dy < 150) {
            Animated.parallel([
              Animated.timing(animated, {
                toValue: 0,
                duration: 150,
                useNativeDriver: false,
              }),
              Animated.timing(animatedMargin, {
                toValue: 0,
                duration: 150,
                useNativeDriver: false,
              }),
            ]).start();
          } else if (dy > 150) {
            Animated.timing(animated, {
              toValue: 400,
              duration: 300,
              useNativeDriver: false,
            }).start();
          }
        },
      }),
    [animated, animatedMargin],
  );

  const spacerStyle = {
    marginTop: animatedMargin,
  };

  const opacityInterpolate = animated.interpolate({
    inputRange: [-400, 0, 400],
    outputRange: [0, 1, 0],
  });

  const modalStyle = {
    transform: [{ translateY: animated }],
    opacity: opacityInterpolate,
  };

  return (
    <View style={styles.container}>
      <Animated.View style={spacerStyle} />
      <Animated.View style={[styles.modal, modalStyle]} {...panResponder.panHandlers}>
        <View style={styles.comments}>
          <ScrollView
            scrollEventThrottle={16}
            onScroll={(event) => {
              scrollOffset.current = event.nativeEvent.contentOffset.y;
              scrollViewHeight.current = event.nativeEvent.layoutMeasurement.height;
            }}
            onContentSizeChange={(__, height) => {
              contentHeight.current = height;
            }}
          >
            <Text style={styles.fakeText}>Top</Text>
            <View style={styles.fakeComments} />
            <Text style={styles.fakeText}>Bottom</Text>
          </ScrollView>
        </View>
        <View style={styles.inputWrap}>
          <TextInput
            style={styles.textInput}
            placeholder="Comment"
            placeholderTextColor="#cacaca"
          />
        </View>
      </Animated.View>
    </View>
  );
};
