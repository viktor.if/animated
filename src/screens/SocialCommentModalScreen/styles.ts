import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  modal: {
    flex: 1,
    borderRadius: 15,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: '#333',
  },
  comments: {
    flex: 1,
  },
  fakeText: {
    padding: 15,
    textAlign: 'center',
  },
  fakeComments: {
    height: 1000,
    backgroundColor: '#f1f1f1',
  },
  inputWrap: {
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  textInput: {
    flex: 1,
    height: 50,
    borderTopWidth: 1,
    borderTopColor: '#000',
  },
});
