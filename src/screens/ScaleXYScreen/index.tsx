import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(1));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    Animated.timing(animation.current, {
      toValue: 2,
      duration: 1500,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 1,
        duration: 1500,
        useNativeDriver: true,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ scaleX: animation.current }],
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
