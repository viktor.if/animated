import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
  },
  background: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    position: 'absolute',
    width: 60,
    height: 60,
    bottom: 20,
    right: 20,
    borderRadius: 300,
  },
  button: {
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 300,
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
  pay: {
    backgroundColor: '#00b15e',
  },
  other: {
    backgroundColor: '#fff',
  },
  payText: {
    color: '#fff',
  },
  label: {
    color: '#fff',
    position: 'absolute',
    fontSize: 18,
    backgroundColor: 'transparent',
  },
});
