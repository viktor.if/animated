import React, { useRef, useState } from 'react';
import { TouchableOpacity, View, Animated, ViewStyle, Text, PanResponder } from 'react-native';
import { Metrics } from '../../styles';
import styles from './styles';

export default () => {
  const [zone, setZone] = useState<string | undefined>('Still Touchable');

  const onPress = () => setZone('I got touched with a parent pan responder');

  const getDirectionAndColor = ({
    moveX,
    moveY,
    dx,
    dy,
  }: {
    moveX: number;
    moveY: number;
    dx: number;
    dy: number;
  }) => {
    const draggedDown = dy > 30;
    const draggedUp = dy < -30;
    const draggedLeft = dx < -30;
    const draggedRight = dx > 30;
    const isRed = moveY < 90 && moveY > 40 && moveX > 0 && moveX < Metrics.screenWidth;
    const isBlue = moveY > Metrics.screenHeight - 50 && moveX > 0 && moveX < Metrics.screenWidth;
    let dragDirection = '';

    if (draggedDown || draggedUp) {
      if (draggedDown) {
        dragDirection += 'dragged down ';
      }
      if (draggedUp) {
        dragDirection += 'dragged up ';
      }
    }

    if (draggedLeft || draggedRight) {
      if (draggedLeft) {
        dragDirection += 'dragged left ';
      }
      if (draggedRight) {
        dragDirection += 'dragged right ';
      }
    }

    if (isRed) {
      return `red ${dragDirection}`;
    }
    if (isBlue) {
      return `blue ${dragDirection}`;
    }
    if (dragDirection) {
      return dragDirection;
    }
  };

  const panResponder = React.useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: (_, gestureState) => !!getDirectionAndColor(gestureState),
      onPanResponderMove: (_, gestureState) => {
        const drag = getDirectionAndColor(gestureState);
        setZone(drag);
      },
      onPanResponderTerminationRequest: () => true,
    }),
  ).current;

  return (
    <View style={styles.container} {...panResponder.panHandlers}>
      <View style={styles.zone1} />
      <View style={styles.center}>
        <TouchableOpacity onPress={onPress}>
          <Text>{zone}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.zone2} />
    </View>
  );
};
