import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
  },
  screenHeader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  screenText: {
    flex: 1,
  },
});
