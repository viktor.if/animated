import React, { useMemo } from 'react';
import { View, Animated, Text, Dimensions, PixelRatio, ScrollView } from 'react-native';

import styles from './styles';
import images from './images';

const getScreen1Styles = (animation: Animated.Value, width: number) => {
  const image2TranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, -100],
    extrapolate: 'clamp',
  });

  return {
    image2: {
      transform: [
        {
          translateX: image2TranslateX,
        },
      ],
    },
  };
};

const getScreen2Styles = (animation: Animated.Value, width: number) => {
  const inputRange = [0, width, width * 2];

  const image2TranslateY = animation.interpolate({
    inputRange,
    outputRange: [100, 0, -100],
    extrapolate: 'clamp',
  });

  const image20Opacity = animation.interpolate({
    inputRange,
    outputRange: [0, 1, 0],
    extrapolate: 'clamp',
  });

  return {
    image2: {
      opacity: image20Opacity,
      transform: [
        {
          translateY: image2TranslateY,
        },
      ],
    },
  };
};

const getScreen3Styles = (animation: Animated.Value, width: number) => {
  const inputRange = [width, width * 2, width * 3];

  const imageScale = animation.interpolate({
    inputRange,
    outputRange: [0, 1, 0],
    extrapolate: 'clamp',
  });

  const image2Rotate = animation.interpolate({
    inputRange,
    outputRange: ['-180deg', '0deg', '180deg'],
    extrapolate: 'clamp',
  });

  return {
    image1: {
      transform: [
        {
          scale: imageScale,
        },
      ],
    },
    image2: {
      transform: [
        {
          scale: imageScale,
        },
        {
          rotate: image2Rotate,
        },
      ],
    },
  };
};

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);

  const { width, height } = Dimensions.get('screen');

  const screen1Styles = getScreen1Styles(animation, width);
  const screen2Styles = getScreen2Styles(animation, width);
  const screen3Styles = getScreen3Styles(animation, width);

  return (
    <View style={styles.screen}>
      <ScrollView
        style={styles.screen}
        pagingEnabled
        horizontal
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: { x: animation },
              },
            },
          ],
          { useNativeDriver: false },
        )}
      >
        {/** Screen 1 */}
        <View style={{ width, height, backgroundColor: '#f89e20' }}>
          <View style={styles.screenHeader}>
            <Animated.Image
              source={images.Image1}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(75),
                height: PixelRatio.getPixelSizeForLayoutSize(63),
              }}
              resizeMode="contain"
            />

            <Animated.Image
              source={images.Image2}
              style={[
                {
                  width: PixelRatio.getPixelSizeForLayoutSize(46),
                  height: PixelRatio.getPixelSizeForLayoutSize(28),
                  position: 'absolute',
                  top: 200,
                  left: 60,
                },
                screen1Styles.image2,
              ]}
              resizeMode="contain"
            />

            <Animated.Image
              source={images.Image3}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(23),
                height: PixelRatio.getPixelSizeForLayoutSize(17),
                position: 'absolute',
                top: 150,
                left: 60,
              }}
              resizeMode="contain"
            />
          </View>

          <View style={styles.screenText}>
            <Text>Screen 1</Text>
          </View>
        </View>

        {/** Screen 2 */}
        <View style={{ width, height, backgroundColor: '#f89e20' }}>
          <View style={styles.screenHeader}>
            <Animated.Image
              source={images.Image1}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(75),
                height: PixelRatio.getPixelSizeForLayoutSize(63),
              }}
              resizeMode="contain"
            />

            <Animated.Image
              source={images.Image2}
              style={[
                {
                  width: PixelRatio.getPixelSizeForLayoutSize(46),
                  height: PixelRatio.getPixelSizeForLayoutSize(28),
                  position: 'absolute',
                  top: 200,
                  left: 60,
                },
                screen2Styles.image2,
              ]}
              resizeMode="contain"
            />

            <Animated.Image
              source={images.Image3}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(23),
                height: PixelRatio.getPixelSizeForLayoutSize(17),
                position: 'absolute',
                top: 150,
                left: 60,
              }}
              resizeMode="contain"
            />
          </View>

          <View style={styles.screenText}>
            <Text>Screen 2</Text>
          </View>
        </View>

        {/** Screen 3 */}
        <View style={{ width, height, backgroundColor: '#f89e20' }}>
          <View style={styles.screenHeader}>
            <Animated.Image
              source={images.Image1}
              style={[
                {
                  width: PixelRatio.getPixelSizeForLayoutSize(75),
                  height: PixelRatio.getPixelSizeForLayoutSize(63),
                },
                screen3Styles.image1,
              ]}
              resizeMode="contain"
            />

            <Animated.Image
              source={images.Image2}
              style={[
                {
                  width: PixelRatio.getPixelSizeForLayoutSize(46),
                  height: PixelRatio.getPixelSizeForLayoutSize(28),
                  position: 'absolute',
                  top: 200,
                  left: 60,
                },
                screen3Styles.image2,
              ]}
              resizeMode="contain"
            />

            <Animated.Image
              source={images.Image3}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(23),
                height: PixelRatio.getPixelSizeForLayoutSize(17),
                position: 'absolute',
                top: 150,
                left: 60,
              }}
              resizeMode="contain"
            />
          </View>

          <View style={styles.screenText}>
            <Text>Screen 3</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
