import React from 'react';
import { View, Dimensions, StyleSheet, Animated, ImageURISource, Text } from 'react-native';

const { width, height } = Dimensions.get('window');

export default ({
  image,
  title,
  translateX,
}: {
  image: ImageURISource;
  title: string;
  translateX: Animated.AnimatedInterpolation;
}) => {
  const animatedStyle = {
    transform: [{ translateX }],
  };

  return (
    <View style={styles.containter}>
      <Animated.Image source={image} style={[styles.image, animatedStyle]} resizeMode="cover" />
      <View style={[StyleSheet.absoluteFill, styles.center]}>
        <View style={styles.textWrap}>
          <Text style={styles.title}>{title}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containter: {
    width,
    height,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  center: {
    justifyContent: 'center',
  },
  textWrap: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    paddingVertical: 10,
  },
  title: {
    backgroundColor: 'transparent',
    fontSize: 30,
    color: '#fff',
    textAlign: 'center',
  },
});
