import React, { useMemo } from 'react';
import { View, Animated, ScrollView, Dimensions } from 'react-native';
import Moment from './Moment';

import styles from './styles';

const { width } = Dimensions.get('window');
const Images = [
  { image: require('../../../assets/images/drink1.jpg'), title: 'Vokda Cran' },
  { image: require('../../../assets/images/drink2.jpg'), title: 'Old Fashion' },
  { image: require('../../../assets/images/drink3.jpg'), title: 'Mule' },
  { image: require('../../../assets/images/drink4.jpg'), title: 'Strawberry Daiquiri' },
];

const getInterpolate = (animatedScroll: Animated.Value, i: number) => {
  const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
  const outputRange = i === 0 ? [0, 0, 150] : [-300, 0, 150];

  return animatedScroll.interpolate({ inputRange, outputRange, extrapolate: 'clamp' });
};

const getSeparator = (i: number) => {
  return <View key={i} style={[styles.separator, { left: (i - 1) * width - 2.5 }]} />;
};

export default () => {
  const animatedScroll = useMemo(() => new Animated.Value(0), []);

  return (
    <View style={styles.container}>
      <ScrollView
        pagingEnabled
        horizontal
        scrollEventThrottle={16}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: animatedScroll } } }], {
          useNativeDriver: false,
        })}
      >
        {Images.map((image, i) => {
          return <Moment key={i} {...image} translateX={getInterpolate(animatedScroll, i)} />;
        })}
        {Array.apply(null, new Array(Images.length + 1)).map((_, i) => getSeparator(i))}
      </ScrollView>
    </View>
  );
};
