import { Dimensions, StyleSheet } from 'react-native';
import { Colors } from '../../styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    width: Dimensions.get('window').width,
  },
  top: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.greyish3,
  },
  box: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    ...StyleSheet.absoluteFillObject,
  },
  boxText: {
    color: 'white',
  },
});
