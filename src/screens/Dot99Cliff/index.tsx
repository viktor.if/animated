import React, { useMemo, useState } from 'react';
import { View, Animated, Text, PanResponder } from 'react-native';

import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.ValueXY(), []);
  const [viewHeight, setViewHeight] = useState<number>(1000);

  const panResponder = useMemo(() => {
    return PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => animation.extractOffset(),
      onPanResponderMove: Animated.event(
        [
          null,
          {
            dx: animation.x,
            dy: animation.y,
          },
        ],
        // PanResponder does not work with native drivers
        { useNativeDriver: false },
      ),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const inputRange = [0, viewHeight / 2 - 50.01, viewHeight / 2 - 50, viewHeight];

  const backgroundColor = animation.y.interpolate({
    inputRange,
    outputRange: ['rgb(99,71,255)', 'rgb(99,71,255)', 'rgb(255,0,0)', 'rgb(255,0,0)'],
  });

  const animatedStyles = {
    transform: [...animation.getTranslateTransform()],
    backgroundColor,
  };

  return (
    <View
      style={styles.container}
      onLayout={({
        nativeEvent: {
          layout: { height },
        },
      }) => setViewHeight(height)}
    >
      <View style={[styles.container, styles.top]}>
        <Text>Good</Text>
      </View>
      <View style={styles.container}>
        <Text>Bad</Text>
      </View>
      <Animated.View style={[styles.box, animatedStyles]} {...panResponder.panHandlers}>
        <Text style={styles.boxText}>Box</Text>
      </Animated.View>
    </View>
  );
};
