import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  head: {
    alignItems: 'center',
    borderColor: '#a2d2ba',
    borderRadius: 1000,
    borderWidth: 0.5,
    height: 80,
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'absolute',
    width: 80,
  },
  text: {
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    transform: [{ translateY: 20 }],
  },
});
