import React, { useMemo } from 'react';
import {
  View,
  Animated,
  Text,
  ImageBackground,
  PanResponder,
  PanResponderInstance,
} from 'react-native';

import styles from './styles';

const ImageBackgroundAnimated = Animated.createAnimatedComponent(ImageBackground);
const TototoImage = require('../../../assets/images/toroto.png');

export default () => {
  const heads = useMemo<{ id: string; image: any; text?: string; animation: Animated.ValueXY }[]>(
    () => [
      {
        id: '_1st',
        image: TototoImage,
        text: 'Drag me',
        animation: new Animated.ValueXY({ x: 0, y: 0 }),
      },
      {
        id: '_2nd',
        image: TototoImage,
        animation: new Animated.ValueXY({ x: 0, y: 0 }),
      },
      {
        id: '_3rd',
        image: TototoImage,
        animation: new Animated.ValueXY({ x: 0, y: 0 }),
      },
      {
        id: '_4th',
        image: TototoImage,
        animation: new Animated.ValueXY({ x: 0, y: 0 }),
      },
    ],
    [],
  );

  const panResponder = useMemo<PanResponderInstance>(
    () => {
      return PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponder: () => true,
        onPanResponderGrant: () => {
          heads.map(({ animation }) => {
            animation.extractOffset();
            // bugfix, extractOffset does NOT stop animation
            animation.setValue({ x: 0, y: 0 });
          });
        },
        onPanResponderMove: (e, { dx, dy }) => {
          heads[0].animation.setValue({ x: dx, y: dy });

          heads.slice(1).map(({ animation }, index) => {
            Animated.sequence([
              Animated.delay(10 * index),
              Animated.spring(animation, { toValue: { x: dx, y: dy }, useNativeDriver: false }),
            ]).start();
          });
        },
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <View style={styles.screen}>
      {heads
        .slice(0)
        .reverse()
        .map((item, index, items) => {
          const pan = index === items.length - 1 ? panResponder.panHandlers : {};

          return (
            <ImageBackgroundAnimated
              {...pan}
              key={item.id}
              source={item.image}
              style={[styles.head, { transform: item.animation.getTranslateTransform() }]}
            >
              <Text style={styles.text}>{item.text}</Text>
            </ImageBackgroundAnimated>
          );
        })}
    </View>
  );
};
