import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    Animated.timing(animation.current, {
      toValue: 300,
      duration: 1500,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 0,
        duration: 1500,
        useNativeDriver: true,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const positionWithOffset = Animated.add(animation.current, 50);

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: animation.current }],
  };

  const animatedStylesWithOffset: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: positionWithOffset }],
  };

  return (
    <View style={styles.screen}>
      <View style={styles.boxContainer}>
        <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
          <Animated.View style={[styles.box, animatedStyles]}>
            <Text style={styles.text}>Press us</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
          <Animated.View style={[styles.box, animatedStylesWithOffset]}>
            <Text style={styles.text}>Press us</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
