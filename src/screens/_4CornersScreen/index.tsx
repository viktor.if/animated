import React, { useMemo, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Text, LayoutChangeEvent } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.ValueXY({ x: 0, y: 0 }), []);
  const [scene, setScene] = useState<{ width: number; height: number }>({ width: 0, height: 0 });

  const onLayout = (e: LayoutChangeEvent) => {
    setScene({
      width: e.nativeEvent.layout.width,
      height: e.nativeEvent.layout.height,
    });
  };

  const startAnimation = () => {
    Animated.sequence([
      Animated.spring(animation.y, { toValue: scene.height - 100, useNativeDriver: true }),
      Animated.spring(animation.x, { toValue: scene.width - 100, useNativeDriver: true }),
      Animated.spring(animation.y, { toValue: 0, useNativeDriver: true }),
      Animated.spring(animation.x, { toValue: 0, useNativeDriver: true }),
    ]).start();
  };

  const animatedStyles = {
    transform: animation.getTranslateTransform(),
  };

  return (
    <View style={styles.screen} {...{ onLayout }}>
      <TouchableWithoutFeedback onPress={startAnimation}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
