import React, { useEffect, useMemo, useState } from 'react';
import {
  View,
  Animated,
  TouchableWithoutFeedback,
  StyleSheet,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';

import styles from './styles';

const Touchable = Animated.createAnimatedComponent(TouchableOpacity);

export default () => {
  const animate = useMemo(() => new Animated.Value(0), []);
  const [success, setSuccess] = useState<boolean>(false);

  const handlePress = () => {
    Animated.timing(animate, {
      toValue: 1,
      duration: 300,
      useNativeDriver: false,
    }).start();
  };

  const handleSend = () => {
    setSuccess(true);
  };

  useEffect(() => {
    if (success) {
      Animated.sequence([
        Animated.timing(animate, { toValue: 0, duration: 300, useNativeDriver: false }),
        Animated.delay(1500),
      ]).start(() => {
        setSuccess(false);
      });
    }
  }, [success, animate]);

  const inputScaleInterpolate = animate.interpolate({
    inputRange: [0, 0.5, 0.6],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const sendButtonInterpolate = animate.interpolate({
    inputRange: [0, 0.6, 1],
    outputRange: [0, 0, 1],
  });

  const notifyTextScaleInterpolate = animate.interpolate({
    inputRange: [0, 0.5],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  const thankyouScaleInterpolate = animate.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0],
  });

  const thankyouTextStyle = {
    transform: [{ scale: thankyouScaleInterpolate }],
  };

  const notifyTextStyle = {
    transform: [{ scale: notifyTextScaleInterpolate }],
  };

  const sendButtonStyle = {
    transform: [
      {
        scale: sendButtonInterpolate,
      },
    ],
  };

  const inputScaleStyle = {
    transform: [{ scale: inputScaleInterpolate }],
  };

  const widthInterpolate = animate.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [150, 150, 300],
    extrapolate: 'clamp',
  });

  const buttonWrapStyle = {
    width: widthInterpolate,
  };

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={handlePress}>
        <Animated.View style={[styles.buttonWrap, buttonWrapStyle]}>
          {!success && (
            <Animated.View style={[StyleSheet.absoluteFill, styles.inputWrap, inputScaleStyle]}>
              <TextInput
                autoFocus
                keyboardType="email-address"
                placeholderTextColor="rgba(255,123,115,0.8)"
                placeholder="Email"
                style={styles.textInput}
              />
              <Touchable style={[styles.sendButton, sendButtonStyle]} onPress={handleSend}>
                <Text style={styles.sendText}>Send</Text>
              </Touchable>
            </Animated.View>
          )}

          {!success && (
            <Animated.View style={notifyTextStyle}>
              <Text style={styles.notifyText}>Notify Me</Text>
            </Animated.View>
          )}

          {success && (
            <Animated.View style={thankyouTextStyle}>
              <Text style={styles.notifyText}>Thank You</Text>
            </Animated.View>
          )}
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
