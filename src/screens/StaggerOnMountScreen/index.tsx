import React, { useEffect, useMemo, useRef } from 'react';
import {
  View,
  Animated,
  Text,
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from './styles';

const Image = require('../../../assets/images/night-sky.jpg');
const TextInputAnimated = Animated.createAnimatedComponent(TextInput);

const createAnimationStyle = (animation: Animated.Value) => {
  const translateY = animation.interpolate({ inputRange: [0, 1], outputRange: [-5, 0] });

  return {
    opacity: animation,
    transform: [{ translateY }],
  };
};

export default () => {
  const ref = useRef<TextInput>(null);

  const email = useMemo(() => new Animated.Value(0), []);
  const password = useMemo(() => new Animated.Value(0), []);
  const button = useMemo(() => new Animated.Value(0), []);

  const emailStyle = createAnimationStyle(email);
  const passwordStyle = createAnimationStyle(password);
  const buttonStyle = createAnimationStyle(button);

  useEffect(() => {
    Animated.stagger(100, [
      Animated.timing(email, { toValue: 1, useNativeDriver: true, duration: 200 }),
      Animated.timing(password, { toValue: 1, useNativeDriver: true, duration: 200 }),
      Animated.timing(button, { toValue: 1, useNativeDriver: true, duration: 200 }),
    ]).start(() => ref.current?.focus());
  }, [email, password, button]);

  return (
    <ImageBackground
      source={Image}
      resizeMode="cover"
      style={[StyleSheet.absoluteFillObject, { width: undefined, height: undefined }]}
    >
      <View style={styles.container} />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        style={styles.form}
      >
        <View style={styles.container}>
          <Text style={styles.title}>Login</Text>
          <TextInputAnimated
            ref={ref}
            style={[styles.input, emailStyle]}
            placeholder="Email"
            keyboardType="email-address"
          />
          <TextInputAnimated
            style={[styles.input, passwordStyle]}
            placeholder="Password"
            secureTextEntry
          />
          <TouchableOpacity>
            <Animated.View style={[styles.button, buttonStyle]}>
              <Text style={styles.buttonText}>Login</Text>
            </Animated.View>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.container} />
    </ImageBackground>
  );
};
