import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
  },
  grid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  gridImage: {
    width: '33%',
    height: 150,
  },
  topContent: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  content: {
    flex: 2,
    backgroundColor: '#fff',
  },
  viewImage: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  title: {
    fontSize: 24,
    margin: 16,
  },
  body: {
    fontSize: 16,
    marginHorizontal: 16,
  },
  close: {
    position: 'absolute',
    top: 20,
    right: 20,
  },
  closeText: {
    backgroundColor: 'transparent',
    fontSize: 28,
    color: '#fff',
  },
});
