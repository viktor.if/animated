import React, { useMemo, useState, useRef, useEffect } from 'react';
import {
  View,
  Animated,
  Text,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import { getDefaultHeaderHeight } from '@react-navigation/elements';
import { useSafeAreaFrame, useSafeAreaInsets } from 'react-native-safe-area-context';

import styles from './styles';
import { images } from './images';

export default () => {
  const frame = useSafeAreaFrame();
  const insets = useSafeAreaInsets();
  const headerHeight = getDefaultHeaderHeight(frame, false, insets.top);

  const imageRefs = useMemo<{ [index: number]: Image }>(() => ({}), []);
  const viewImageRef = useRef<View>(null);
  const [activeImage, setActiveImage] = useState<number | null>(null);
  const [activeIndex, setActiveIndex] = useState<number | null>(null);

  const size = useMemo(() => new Animated.ValueXY({ x: 0, y: 0 }), []);
  const position = useMemo(() => new Animated.ValueXY({ x: 0, y: 0 }), []);
  const animation = useMemo(() => new Animated.Value(0), []);

  let _x = useRef<number>(0);
  let _y = useRef<number>(0);
  let _width = useRef<number>(0);
  let _height = useRef<number>(0);

  const handleOpenImage = (index: number) => {
    setActiveImage(images[index]);
    setActiveIndex(index);
  };

  const handleClose = () => {
    Animated.parallel([
      Animated.timing(position.y, {
        toValue: _y.current,
        duration: 250,
        useNativeDriver: false,
      }),
      Animated.timing(position.x, {
        toValue: _x.current,
        duration: 250,
        useNativeDriver: false,
      }),
      Animated.timing(size.x, {
        toValue: _width.current,
        duration: 250,
        useNativeDriver: false,
      }),
      Animated.timing(size.y, {
        toValue: _height.current,
        duration: 250,
        useNativeDriver: false,
      }),
      Animated.timing(animation, {
        toValue: 0,
        duration: 250,
        useNativeDriver: false,
      }),
    ]).start(() => {
      setActiveImage(null);
      setActiveIndex(null);
    });
  };

  useEffect(() => {
    if (activeIndex !== null) {
      imageRefs?.[activeIndex].measure((x, y, width, height, pageX, pageY) => {
        _x.current = pageX;
        _y.current = pageY - headerHeight;
        _width.current = width;
        _height.current = height;

        position.setValue({ x: pageX, y: pageY });

        size.setValue({ x: width, y: height });

        viewImageRef.current?.measure((tX, tY, tWidth, tHeight, tPageX) => {
          Animated.parallel([
            Animated.spring(position.y, {
              toValue: 0,
              useNativeDriver: false,
            }),
            Animated.spring(position.x, {
              toValue: tPageX,
              useNativeDriver: false,
            }),
            Animated.spring(size.x, {
              toValue: tWidth,
              useNativeDriver: false,
            }),
            Animated.spring(size.y, {
              toValue: tHeight,
              useNativeDriver: false,
            }),
            Animated.spring(animation, {
              toValue: 1,
              useNativeDriver: false,
            }),
          ]).start();
        });
      });
    }
  }, [activeIndex, imageRefs, position, size, animation, headerHeight]);

  const animatedContentTranslate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [300, 0],
  });

  const animatedContentStyle = {
    opacity: animation,
    transform: [{ translateY: animatedContentTranslate }],
  };

  const activeImageStyle = {
    width: size.x,
    height: size.y,
    top: position.y,
    left: position.x,
  };

  const activeIndexStyle = {
    opacity: activeImage ? 0 : 1,
  };

  const animatedCloseStyle = {
    opacity: animation,
  };

  return (
    <View style={styles.screen}>
      <ScrollView style={styles.screen} showsVerticalScrollIndicator={false}>
        <View style={styles.grid}>
          {images.map((src: any, index) => {
            const style = index === activeIndex ? activeIndexStyle : undefined;
            return (
              <TouchableWithoutFeedback key={index} onPress={() => handleOpenImage(index)}>
                <Image
                  source={src}
                  resizeMode="cover"
                  style={[styles.gridImage, style]}
                  // @ts-ignore
                  ref={(ref) => (imageRefs[index] = ref)}
                />
              </TouchableWithoutFeedback>
            );
          })}
        </View>
      </ScrollView>

      <View style={StyleSheet.absoluteFill} pointerEvents={activeImage ? 'auto' : 'none'}>
        <View style={styles.topContent} ref={viewImageRef} onLayout={() => {}}>
          <Animated.Image
            key={activeImage}
            // @ts-ignore
            source={activeImage}
            resizeMode="cover"
            style={[styles.viewImage, activeImageStyle]}
          />
        </View>
        <Animated.View style={[styles.content, animatedContentStyle]}>
          <Text style={styles.title}>Pretty images from Unsplash</Text>
          <Text style={styles.body}>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
            dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus
          </Text>
        </Animated.View>
      </View>
      <TouchableWithoutFeedback onPress={handleClose}>
        <Animated.View style={[styles.close, animatedCloseStyle]}>
          <Text style={styles.closeText}>X</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
