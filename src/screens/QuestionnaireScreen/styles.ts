import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#e22d4b',
    flexDirection: 'row',
  },
  option: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  optionText: {
    fontSize: 30,
    color: '#fff',
    marginBottom: 50,
  },
  yes: {
    backgroundColor: 'rgba(255,255,255,0.1)',
  },
  overlay: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  questionText: {
    backgroundColor: 'transparent',
    fontSize: 30,
    color: '#fff',
    textAlign: 'center',
    position: 'absolute',
    marginHorizontal: 16,
  },
  progress: {
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    height: 10,
  },
  bar: {
    height: '100%',
    backgroundColor: '#fff',
  },
});
