import React, { useMemo, useState } from 'react';
import { View, Animated, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';

import styles from './styles';

const questions: string[] = [
  'Do you tend to follow directions when given?',
  'Are you comfortable with the idea of standing and doing light physical activity most of the day?',
  'Would you enjoy making sure your customers leave happy?',
  'Are you willing to work nights and weekends (and possibly holidays)?',
];

const { width } = Dimensions.get('screen');

export default () => {
  const [index, setIndex] = useState<number>(0);
  const animation = useMemo(() => new Animated.Value(0), []);
  const progress = useMemo(() => new Animated.Value(0), []);

  const question = questions[index];
  let nextQuestion: string = '';

  const mainQuestionInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -width],
  });

  const nextQuestionInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [width, 0],
  });

  const mainStyle = {
    transform: [{ translateX: mainQuestionInterpolate }],
  };

  const nextStyle = {
    transform: [{ translateX: nextQuestionInterpolate }],
  };

  const progressInterpolate = progress.interpolate({
    inputRange: [0, questions.length],
    outputRange: ['0%', '100%'],
  });

  const progressStyle = { width: progressInterpolate };

  if (index + 1 < questions.length) {
    nextQuestion = questions[index + 1];
  }

  const handlePress = () => {
    Animated.parallel([
      Animated.timing(animation, {
        toValue: 1,
        duration: 400,
        useNativeDriver: false,
      }),
      Animated.timing(progress, {
        toValue: index + 1,
        duration: 400,
        useNativeDriver: false,
      }),
    ]).start(() => {
      setIndex((prevIndex) => prevIndex + 1);
      animation.setValue(0);
    });
  };

  return (
    <View style={styles.screen}>
      <View style={[StyleSheet.absoluteFill, styles.overlay]}>
        <Animated.Text style={[styles.questionText, mainStyle]}>{question}</Animated.Text>
        <Animated.Text style={[styles.questionText, nextStyle]}>{nextQuestion}</Animated.Text>
      </View>

      <View style={styles.progress}>
        <Animated.View style={[styles.bar, progressStyle]} />
      </View>

      <TouchableOpacity onPress={handlePress} activeOpacity={0.7} style={styles.option}>
        <Text style={styles.optionText}>No</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={handlePress}
        activeOpacity={0.7}
        style={[styles.option, styles.yes]}
      >
        <Text style={styles.optionText}>Yes</Text>
      </TouchableOpacity>
    </View>
  );
};
