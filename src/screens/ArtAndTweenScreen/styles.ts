import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  svg: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
  },
  text: {
    color: '#000',
    fontWeight: 'bold',
    marginVertical: 8,
    textTransform: 'uppercase',
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignSelf: 'center',
  },
});
