import React, { useMemo, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const [toggle, setToggle] = useState<boolean>(true);

  let pressed = useMemo(() => false, []);

  const handlePress = () => {
    const toValue = pressed ? 0 : 1;
    Animated.timing(animation, {
      toValue,
      useNativeDriver: false,
      duration: 1000,
    }).start();
    pressed = !pressed;
  };

  const animatedStyles = {
    backgroundColor: animation.interpolate({
      inputRange: [0, 1],
      outputRange: ['rgb(255,99,71)', 'rgb(99,71,255)'],
    }),
  };

  const handleToggle = () => setToggle((prev) => !prev);

  return (
    <View style={styles.screen}>
      <View>
        <TouchableWithoutFeedback onPress={handlePress}>
          <Animated.View style={[styles.box, animatedStyles]}>
            <Text style={styles.text}>Press me</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <View style={styles.cover} pointerEvents={toggle ? 'none' : 'auto'} />
      </View>

      <TouchableOpacity onPress={handleToggle}>
        <View style={styles.toggle}>
          <Text>Pointer Events are {toggle ? 'OFF' : 'ON'}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
