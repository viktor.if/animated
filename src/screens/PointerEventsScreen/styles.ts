import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../styles';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: Metrics.screenWidth,
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: 'tomato',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: Colors.white,
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  cover: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
  },
  toggle: {
    marginTop: 24,
  },
});
