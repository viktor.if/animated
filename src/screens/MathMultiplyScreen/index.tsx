import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animationValue = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    Animated.timing(animationValue.current, {
      toValue: 300,
      duration: 1500,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animationValue.current, {
        toValue: 0,
        duration: 1500,
        useNativeDriver: true,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const translationOrigin: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: animationValue.current }],
  };

  const animationValueModified = Animated.multiply(animationValue.current, 2);

  const translationModified: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: animationValueModified }],
  };

  return (
    <View style={styles.screen}>
      <View style={styles.boxContainer}>
        <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
          <Animated.View style={[styles.box, translationOrigin]}>
            <Text style={styles.text}>Press us</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
          <Animated.View style={[styles.box, translationModified]}>
            <Text style={styles.text}>Press us</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
