import { StyleSheet } from 'react-native';
import { Colors } from '../../styles';

export default StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  box: {
    alignItems: 'center',
    backgroundColor: 'tomato',
    height: 160,
    justifyContent: 'center',
    width: 160,
  },
  text: {
    color: Colors.white,
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontWeight: 'bold',
    marginVertical: 8,
    textTransform: 'uppercase',
  },
});
