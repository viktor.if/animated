import React, { useMemo, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Text } from 'react-native';
import styles from './styles';

export default () => {
  const opacity = useMemo(() => new Animated.Value(1), []);
  const animation = useMemo(() => new Animated.Value(1), []);
  const [textBox, setTextBox] = useState<string>('Press me');

  const startAnimation = () => {
    setTextBox('Catch me');
    Animated.parallel(
      [
        Animated.timing(animation, {
          toValue: 300,
          duration: 500,
          useNativeDriver: true,
        }),
        Animated.timing(opacity, {
          toValue: 0,
          duration: 5000,
          useNativeDriver: true,
        }),
      ],
      { stopTogether: false },
    ).start(({ finished }) => {
      if (!finished) {
        setTimeout(() => {
          Animated.spring(animation, { toValue: 0, useNativeDriver: true }).start();
          Animated.spring(opacity, { toValue: 1, useNativeDriver: true }).start(() =>
            setTextBox('Press me'),
          );
        }, 0);
      }
    });
  };

  const animatedStyles = {
    opacity,
    transform: [
      {
        translateY: animation,
      },
    ],
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>{textBox}</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
