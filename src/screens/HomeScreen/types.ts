import { Pages } from '../../navigators/Routes';

export type Page = {
  title: string;
  route: Pages;
};
