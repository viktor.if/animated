import React from 'react';
import { View, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import AnimationListItem from './components/AnimationListItem';
import styles from './styles';
import { pages } from './data';

export default () => {
  const navigation = useNavigation<any>();
  const insets = useSafeAreaInsets();

  return (
    <View style={styles.screen}>
      <FlatList
        contentContainerStyle={[styles.list, { paddingBottom: insets.bottom }]}
        data={pages}
        keyExtractor={(page) => page.title}
        renderItem={({ item }) => (
          <AnimationListItem title={item.title} onPress={() => navigation.navigate(item.route)} />
        )}
      />
    </View>
  );
};
