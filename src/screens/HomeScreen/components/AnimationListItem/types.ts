import { GestureResponderEvent } from 'react-native';

export type AnimationListItemProps = {
  title: string;
  onPress: (event: GestureResponderEvent) => void;
};
