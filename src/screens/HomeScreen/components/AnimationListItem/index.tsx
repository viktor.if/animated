import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { AnimationListItemProps } from './types';
import styles from './styles';
import ChevronRightIcon from '../../../../../assets/svg/chevron-right.svg';

export default ({ onPress, title }: AnimationListItemProps) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={styles.title} numberOfLines={1}>
        {title}
      </Text>
      <ChevronRightIcon width={7} height={12} style={styles.icon} fill="#000000" />
    </TouchableOpacity>
  );
};
