import { StyleSheet } from 'react-native';
import { Colors } from '../../../../styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    borderBottomColor: Colors.greyish3,
    borderBottomWidth: 0.5,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 16,
    paddingVertical: 12,
  },
  title: {
    flex: 1,
    flexDirection: 'row',
    fontSize: 17,
    fontStyle: 'normal',
    fontWeight: '400',
    letterSpacing: -0.41,
    lineHeight: 22,
  },
  icon: {
    marginRight: 16,
    marginLeft: 16,
  },
});
