import React, { useMemo, useState } from 'react';
import { View, Animated, TouchableWithoutFeedback } from 'react-native';
import Heart from './Heart';

import styles from './styles';

export default () => {
  const [liked, setLiked] = useState<boolean>(false);
  const scale = useMemo(() => new Animated.Value(0), []);

  const triggerLike = () => {
    setLiked((prev) => !prev);

    Animated.spring(scale, { toValue: 2, friction: 3, useNativeDriver: false }).start(() => {
      scale.setValue(0);
    });
  };

  const bouncyHeart = scale.interpolate({
    inputRange: [0, 1, 2],
    outputRange: [1, 0.8, 1],
  });

  const heartButtonStyle = {
    transform: [{ scale: bouncyHeart }],
  };

  return (
    <View style={styles.container}>
      <View>
        <TouchableWithoutFeedback onPress={triggerLike}>
          <Animated.View style={heartButtonStyle}>
            <Heart filled={liked} />
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
