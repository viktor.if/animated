import React from 'react';
import { StyleSheet, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Heart = ({ filled }: { filled: boolean }) => {
  return (
    <Animated.View style={[styles.heart]}>
      {!filled ? (
        <Icon name="heart-outline" size={45} color="#e31745" />
      ) : (
        <Icon name="heart" size={45} color="#e31745" />
      )}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  heart: {
    width: 50,
    height: 50,
    backgroundColor: 'transparent',
  },
  heartShape: {
    width: 30,
    height: 45,
    position: 'absolute',
    top: 0,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  filledHeart: {
    backgroundColor: '#e31745',
  },
  fit: {
    transform: [{ scale: 0.9 }],
  },
  emptyFill: {
    backgroundColor: '#FFF',
  },
  empty: {
    backgroundColor: '#ccc',
  },
  leftHeart: {
    transform: [{ rotate: '-45deg' }],
    left: 5,
  },
  rightHeart: {
    transform: [{ rotate: '45deg' }],
    right: 5,
  },
});

export default Heart;
