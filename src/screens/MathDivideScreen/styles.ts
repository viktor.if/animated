import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../styles';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 160,
    height: 160,
    backgroundColor: 'tomato',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: Metrics.screenWidth,
  },
  text: {
    color: Colors.white,
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
});
