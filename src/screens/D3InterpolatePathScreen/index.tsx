import React, { useEffect, useMemo, useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Easing, Text } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import { interpolatePath } from 'd3-interpolate-path';

import styles from './styles';

const startPath = 'M45,50a5,5 0 1,0 10, 0a5,5 0 1,0 -10,0';
const endPath = 'M20,50a30,30 0 1,0 60,0a30,30 0 1,0 -60, 0';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);
  const ref = useRef<View>(null);

  const handlePress = () => {
    setIsAnimationRunning(true);
    Animated.sequence([
      Animated.spring(animation, {
        delay: 150,
        toValue: 1,
        friction: 2,
        tension: 30,
        useNativeDriver: true,
      }),
      Animated.delay(1500),
      Animated.timing(animation, { toValue: 0, duration: 500, useNativeDriver: true }),
    ]).start(() => setIsAnimationRunning(false));
  };

  useEffect(() => {
    const _interpolatePath = interpolatePath(startPath, endPath);

    const listenerId = animation.addListener(({ value }) => {
      const path = _interpolatePath(value);

      ref.current?.setNativeProps({ d: path });
    });

    return () => animation.removeListener(listenerId);
  }, [animation, ref]);

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={handlePress} disabled={isAnimationRunning}>
        <View>
          <Svg width={100} height={100} style={styles.svg}>
            <Path d={startPath} stroke="black" ref={ref} fill="black" />
          </Svg>
          <Text style={styles.text}>Press me</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};
