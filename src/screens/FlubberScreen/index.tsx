import React, { useEffect, useMemo, useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Text } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import { interpolate } from 'flubber';

import styles from './styles';

const startPath =
  'M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z';
const endPath =
  'M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);
  const ref = useRef<View>(null);

  const handlePress = () => {
    setIsAnimationRunning(true);
    Animated.sequence([
      Animated.timing(animation, { toValue: 1, duration: 500, useNativeDriver: false }),
      Animated.delay(1500),
      Animated.timing(animation, { toValue: 0, duration: 500, useNativeDriver: false }),
    ]).start(() => setIsAnimationRunning(false));
  };

  useEffect(() => {
    const interpolatePath = interpolate(startPath, endPath, {
      maxSegmentLength: 1,
    });

    const listenerId = animation.addListener(({ value }) => {
      const path = interpolatePath(value);
      ref.current?.setNativeProps({ d: path });
    });

    return () => animation.removeListener(listenerId);
  }, [animation, ref]);

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={handlePress} disabled={isAnimationRunning}>
        <View>
          <Svg width={100} height={100} style={styles.svg}>
            <Path d={startPath} stroke="black" ref={ref} fill="black" scale="2.5" />
          </Svg>
          <Text style={styles.text}>Press me</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};
