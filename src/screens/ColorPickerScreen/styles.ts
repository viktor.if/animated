import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    minWidth: '50%',
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  row: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  colorRowWrap: {
    flexDirection: 'row',
    flex: 1,
    paddingLeft: 5,
  },
  input: {
    flex: 1,
    color: '#000',
    padding: 0,
    fontWeight: '500',
    fontSize: 16,
  },
  okayButton: {
    borderRadius: 100,
    width: 40,
    height: '100%',
    backgroundColor: '#309eeb',
    alignItems: 'center',
    justifyContent: 'center',
  },
  okayText: {
    color: '#fff',
  },
  button: {
    marginTop: 50,
  },
  colorBall: {
    width: 15,
    height: 15,
    borderRadius: 100,
  },
});
