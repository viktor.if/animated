import React, { useMemo, useState, useRef, useEffect } from 'react';
import styles from './styles';
import Icon from 'react-native-vector-icons/Foundation';

import {
  View,
  Animated,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const TextInputAnimated = Animated.createAnimatedComponent(TextInput);
const IconAnimated = Animated.createAnimatedComponent(Icon);

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const buttonAnimation = useMemo(() => new Animated.Value(0), []);
  const [color, setColor] = useState<string>('#000');
  const _open = useRef<boolean>(false);
  const _inputOpen = useRef<boolean>(false);
  const _input = useRef<TextInput>(null);
  const [inputOpen, setInputOpen] = useState<boolean>(false);

  const handleToggle = () => {
    const toValue = _open.current ? 0 : 1;
    Animated.spring(animation, {
      toValue,
      useNativeDriver: false,
    }).start();

    _open.current = !_open.current;
  };

  const toggleInput = () => {
    const toValue = _inputOpen.current ? 0 : 1;
    Animated.timing(buttonAnimation, { toValue, duration: 350, useNativeDriver: false }).start();

    _inputOpen.current = !_inputOpen.current;
    setInputOpen(_inputOpen.current);
  };

  const scaleXinterpolate = animation.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0, 0, 1],
  });

  const translateYinterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [150, 0],
  });

  const rowStyle = {
    opacity: animation,
    transform: [
      { scaleX: scaleXinterpolate },
      { scaleY: animation },
      { translateY: translateYinterpolate },
    ],
  };

  const moveInterpolate = buttonAnimation.interpolate({
    inputRange: [0, 1],
    outputRange: [-150, 0],
  });

  const buttonStyle = {
    transform: [{ translateX: moveInterpolate }, { scale: buttonAnimation }],
  };

  const inputOpacityInterpolate = buttonAnimation.interpolate({
    inputRange: [0, 0.8, 1],
    outputRange: [0, 0, 1],
  });

  const iconTranslate = buttonAnimation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -20],
  });

  const opacityIconInterpolate = buttonAnimation.interpolate({
    inputRange: [0, 0.2],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  const iconStyle = {
    opacity: opacityIconInterpolate,
    transform: [{ translateX: iconTranslate }],
  };

  const inputStyle = {
    opacity: inputOpacityInterpolate,
  };

  const colorStyle = { backgroundColor: color };

  useEffect(() => {
    if (!inputOpen) {
      _input.current?.blur();
    } else {
      _input.current?.focus();
    }
  }, [inputOpen]);

  return (
    <View style={styles.screen}>
      <Animated.View style={[styles.rowWrap, rowStyle]}>
        <TouchableWithoutFeedback onPress={toggleInput}>
          <Animated.View style={[styles.colorBall, colorStyle]} />
        </TouchableWithoutFeedback>

        <View style={styles.row}>
          {/** Bold */}
          <TouchableOpacity>
            <IconAnimated size={30} color="#555" name="bold" style={iconStyle} />
          </TouchableOpacity>
          {/** Italic */}
          <TouchableOpacity>
            <IconAnimated size={30} color="#555" name="italic" style={iconStyle} />
          </TouchableOpacity>
          {/** Align center */}
          <TouchableOpacity>
            <IconAnimated size={30} color="#555" name="align-center" style={iconStyle} />
          </TouchableOpacity>
          {/** Link */}
          <TouchableOpacity>
            <IconAnimated size={30} color="#555" name="link" style={iconStyle} />
          </TouchableOpacity>

          <Animated.View
            style={[StyleSheet.absoluteFill, styles.colorRowWrap]}
            pointerEvents={inputOpen ? 'auto' : 'none'}
          >
            <TextInputAnimated
              style={[styles.input, inputStyle]}
              value={color}
              onChangeText={(_color) => setColor(_color)}
              ref={_input}
            />
            <TouchableWithoutFeedback onPress={toggleInput}>
              <Animated.View style={[styles.okayButton, buttonStyle]}>
                <Text style={styles.okayText}>OK</Text>
              </Animated.View>
            </TouchableWithoutFeedback>
          </Animated.View>
        </View>
      </Animated.View>

      <TouchableOpacity onPress={handleToggle} style={styles.button}>
        <Text>Toggle Open/Closed</Text>
      </TouchableOpacity>
    </View>
  );
};
