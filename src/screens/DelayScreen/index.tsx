import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const colorAnimation = useRef<Animated.Value>(new Animated.Value(0));
  const scaleAnimation = useRef<Animated.Value>(new Animated.Value(1));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);

    Animated.sequence([
      Animated.timing(colorAnimation.current, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }),
      Animated.timing(scaleAnimation.current, {
        toValue: 2,
        duration: 1000,
        useNativeDriver: false,
      }),
      Animated.delay(500),
      Animated.parallel([
        Animated.timing(colorAnimation.current, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(scaleAnimation.current, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
      ]),
    ]).start(() => setIsAnimationRunning(false));
  };

  const backgroundColor = colorAnimation.current.interpolate({
    inputRange: [0, 1],
    outputRange: ['rgb(255,99,71)', 'rgb(99,71,255)'],
  });

  const styleBox: Animated.WithAnimatedObject<ViewStyle> = {
    backgroundColor,
    transform: [{ scale: scaleAnimation.current }],
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, styleBox]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
