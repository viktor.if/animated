import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    Animated.timing(animation.current, {
      toValue: -160,
      duration: 1500,
      useNativeDriver: false,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 0,
        duration: 1500,
        useNativeDriver: false,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    top: animation.current,
  };

  return (
    <View style={styles.screen}>
      <View style={[styles.box, { backgroundColor: 'cyan' }]} />
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
