import React, { useRef, useState } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Animated,
  LayoutChangeEvent,
  ViewStyle,
  Text,
} from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [containerHeight, setContainerHeight] = useState<number>(0);
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    Animated.timing(animation.current, {
      toValue: (containerHeight - 160) / 2,
      duration: 1500,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 0,
        duration: 1500,
        useNativeDriver: true,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const onLayout = (event: LayoutChangeEvent): void => {
    setContainerHeight(event.nativeEvent.layout.height);
  };

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: animation.current }],
  };

  return (
    <View style={styles.screen} onLayout={onLayout}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
