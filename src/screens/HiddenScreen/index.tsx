import React, { useMemo, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.Value(1), []);
  const [isVisible, setIsVisible] = useState<boolean>(true);
  const [textBox, setTextBox] = useState<string>('Press me');

  const startAnimation = () => {
    setTextBox('Catch me');
    Animated.timing(animation, {
      toValue: 0,
      duration: 1500,
      useNativeDriver: true,
    }).start(({ finished }) => {
      setTimeout(() => {
        if (finished) {
          setIsVisible(false);
        } else {
          Animated.spring(animation, { toValue: 1, useNativeDriver: true }).start(() =>
            setTextBox('Press me'),
          );
        }
      }, 0);
    });
  };

  const translateY = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [500, 0],
  });

  const animatedStyles = {
    opacity: animation,
    transform: [
      {
        translateY,
      },
    ],
  };

  return (
    <View style={styles.screen}>
      {isVisible && (
        <TouchableWithoutFeedback onPress={startAnimation}>
          <Animated.View style={[styles.box, animatedStyles]}>
            <Text style={styles.text}>{textBox}</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      )}
    </View>
  );
};
