import React, { useEffect, useMemo, useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text, StyleProp } from 'react-native';
import { interpolateNumber, interpolateRgb } from 'd3-interpolate';

import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);
  const ref = useRef<View>(null);

  const handlePress = () => {
    setIsAnimationRunning(true);
    Animated.timing(animation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start(() => {
      Animated.spring(animation, {
        delay: 150,
        toValue: 0,
        friction: 2,
        tension: 40,
        useNativeDriver: true,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  useEffect(() => {
    const interpolatePosition = interpolateNumber(0, 200);
    const interpolateColor = interpolateRgb('rgb(255,99,71)', 'rgb(99,71,255)');

    const listenerId = animation.addListener(({ value }) => {
      const position = interpolatePosition(value);
      const color = interpolateColor(value);

      const style: StyleProp<ViewStyle> = [
        styles.box,
        {
          backgroundColor: color,
          transform: [
            {
              translateY: position,
            },
          ],
        },
      ];

      ref.current?.setNativeProps({ style });
    });

    return () => animation.removeListener(listenerId);
  }, [animation, ref]);

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={handlePress} disabled={isAnimationRunning}>
        <View ref={ref} style={styles.box}>
          <Text style={styles.text}>Press me</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};
