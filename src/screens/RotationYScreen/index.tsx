import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    animation.current.setValue(0);
    Animated.timing(animation.current, {
      toValue: 360,
      duration: 3000,
      useNativeDriver: true,
    }).start(() => setIsAnimationRunning(false));
  };

  const interpolatedBackgroundColor = animation.current.interpolate({
    inputRange: [0, 360],
    outputRange: ['0deg', '360deg'],
  });

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ rotateY: interpolatedBackgroundColor }],
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
