import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(200));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    Animated.timing(animation.current, {
      toValue: 300,
      duration: 1000,
      useNativeDriver: false,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 200,
        duration: 1000,
        useNativeDriver: false,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    height: animation.current,
  };

  return (
    <View style={styles.screen}>
      <View style={[styles.box, { backgroundColor: 'cyan' }]} />
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
