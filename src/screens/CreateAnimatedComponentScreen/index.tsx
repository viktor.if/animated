import React, { useMemo } from 'react';
import { View, Animated, Button } from 'react-native';
import styles from './styles';

const AnimatedButton = Animated.createAnimatedComponent(Button);

export default () => {
  const animatedColor = useMemo(() => new Animated.Value(0), []);

  const changeColor = () => {
    Animated.timing(animatedColor, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: false,
    }).start(() => {
      Animated.timing(animatedColor, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start();
    });
  };

  const interpolatedColor = animatedColor.interpolate({
    inputRange: [0, 1],
    outputRange: ['rgb(255,99,71)', 'rgb(99,71,255)'],
  });

  return (
    <View style={styles.screen}>
      <AnimatedButton title="Press Me" onPress={changeColor} color={interpolatedColor} />
    </View>
  );
};
