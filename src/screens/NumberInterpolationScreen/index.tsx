import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);

    Animated.timing(animation.current, {
      toValue: 1,
      duration: 1500,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 2,
        duration: 250,
        useNativeDriver: true,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const animatedInterpolate = animation.current.interpolate({
    inputRange: [0, 1, 2],
    outputRange: [0, 250, 0],
  });

  const interpolatedInterpolate = animatedInterpolate.interpolate({
    inputRange: [0, 300],
    outputRange: [1, 0.5],
  });

  const translateXInterpolate = animatedInterpolate.interpolate({
    inputRange: [0, 40, 60, 80, 200, 300],
    outputRange: [0, 80, 10, -10, 50, -40],
  });

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: animatedInterpolate }, { translateX: translateXInterpolate }],
    opacity: interpolatedInterpolate,
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
