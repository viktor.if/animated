import React, { useMemo, useRef, useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  View,
  Animated,
  Text,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';

import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const _input = useRef<TextInput>(null);
  const _open = useRef<boolean>(false);
  const [open, setOpen] = useState<boolean>(false);

  const { width } = Dimensions.get('window');

  const widthInterpolate = animation.interpolate({
    inputRange: [0, 0.5],
    outputRange: [100, width - 40],
    extrapolate: 'clamp',
  });

  const opacityToolbarInterpolate = animation.interpolate({
    inputRange: [0, 0.5],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });

  const toolbarStyles = {
    opacity: opacityToolbarInterpolate,
  };

  const editorHeightInterpolate = animation.interpolate({
    inputRange: [0.7, 1],
    outputRange: [0, 150],
    extrapolate: 'clamp',
  });

  const editorStyle = {
    opacity: animation,
    height: editorHeightInterpolate,
  };

  const opacityButtonInterpolate = animation.interpolate({
    inputRange: [0, 0.5],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  const buttonStyles = {
    opacity: opacityButtonInterpolate,
  };

  const toggleTransform = () => {
    const toValue = _open.current ? 0 : 1;

    Animated.timing(animation, { toValue, duration: 550, useNativeDriver: false }).start(() => {
      _open.current ? _input.current?.blur() : _input.current?.focus();
      _open.current = !_open.current;
      setOpen(_open.current);
    });
  };

  return (
    <View style={styles.screen}>
      <KeyboardAvoidingView
        style={styles.center}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      >
        <Animated.View style={[styles.editor, { width: widthInterpolate }]}>
          <View style={styles.bar}>
            <Animated.View style={[styles.toolbar, toolbarStyles]}>
              <Icon name="format-bold" color="#FFF" size={20} />
              <Icon name="format-italic" color="#FFF" size={20} />
              <Icon name="format-underline" color="#FFF" size={20} />
              <Icon name="format-list-bulleted" color="#FFF" size={20} />
              <Icon name="format-list-numbered" color="#FFF" size={20} />
              <View style={styles.rightInnerBar}>
                <Icon name="link" color="#FFF" size={20} />
                <Icon name="image" color="#FFF" size={20} />
                <Icon name="arrow-down-bold-box" color="#FFF" size={20} />
              </View>
            </Animated.View>

            <Animated.View
              style={[StyleSheet.absoluteFill, styles.center, buttonStyles]}
              pointerEvents={open ? 'none' : 'auto'}
            >
              <TouchableWithoutFeedback onPress={toggleTransform}>
                <View>
                  <Text style={styles.buttonText}>Write</Text>
                </View>
              </TouchableWithoutFeedback>
            </Animated.View>
          </View>

          <Animated.View style={[styles.lowerView, editorStyle]}>
            <TextInput
              placeholder="Start writing..."
              multiline
              ref={_input}
              style={[StyleSheet.absoluteFill, styles.input]}
              placeholderTextColor="#dadada"
              textAlignVertical="top"
            />
          </Animated.View>
        </Animated.View>

        <TouchableWithoutFeedback onPress={toggleTransform}>
          <Animated.View style={[toolbarStyles]}>
            <Text style={styles.close}>Close</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </View>
  );
};
