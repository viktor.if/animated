import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  editor: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.1)',
  },
  bar: {
    height: 50,
    backgroundColor: '#2979ff',
    justifyContent: 'center',
  },
  toolbar: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  rightInnerBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
  },
  lowerView: {
    height: 150,
  },
  input: {
    padding: 10,
    fontSize: 20,
    color: '#000',
  },
  buttonText: {
    color: '#fff',
  },
  close: {
    color: '#2979ff',
    marginTop: 10,
    marginBottom: 20,
  },
});
