import React, { useMemo, useState } from 'react';
import {
  View,
  Animated,
  Text,
  PanResponder,
  PanResponderInstance,
  TouchableOpacity,
} from 'react-native';
import clamp from 'clamp';

import styles from './styles';

const CatImage1 = require('../../../assets/images/cat1.jpg');
const CatImage2 = require('../../../assets/images/cat2.jpg');
const CatImage3 = require('../../../assets/images/cat3.jpg');
const CatImage4 = require('../../../assets/images/cat4.jpg');

const SWIPE_THRESHOLD = 120;

export default () => {
  const [cats, setCats] = useState<{ id: string; image: any; text?: string }[]>([
    {
      id: '_1st',
      image: CatImage1,
      text: 'Sweet Cat',
    },
    {
      id: '_2nd',
      image: CatImage2,
      text: 'Sweeter Cat',
    },
    {
      id: '_3rd',
      image: CatImage3,
      text: 'Sweetest Cat',
    },
    {
      id: '_4th',
      image: CatImage4,
      text: 'Aww',
    },
  ]);

  const _animation = useMemo(() => new Animated.ValueXY({ x: 0, y: 0 }), []);
  const _opacity = useMemo(() => new Animated.Value(1), []);
  const _next = useMemo(() => new Animated.Value(0.9), []);

  const transitionNext = () => {
    Animated.parallel([
      Animated.timing(_opacity, {
        toValue: 0,
        useNativeDriver: true,
        duration: 300,
      }),
      Animated.spring(_next, {
        toValue: 1,
        friction: 4,
        useNativeDriver: true,
      }),
    ]).start(() => {
      setCats((prev) => prev.slice(1));
      _next.setValue(0.9);
      _opacity.setValue(1);
      _animation.setValue({ x: 0, y: 0 });
    });
  };

  const handleNo = () => {
    Animated.timing(_animation.x, { toValue: -SWIPE_THRESHOLD, useNativeDriver: true }).start(
      transitionNext,
    );
  };

  const handleYes = () => {
    Animated.timing(_animation.x, { toValue: SWIPE_THRESHOLD, useNativeDriver: true }).start(
      transitionNext,
    );
  };

  const panResponder = useMemo<PanResponderInstance>(
    () => {
      return PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponder: () => true,
        onPanResponderMove: Animated.event(
          [
            null,
            {
              dx: _animation.x,
              dy: _animation.y,
            },
          ],
          { useNativeDriver: false },
        ),
        onPanResponderRelease: (event, { dx, vx, vy }) => {
          let velocity = 0;

          if (vx >= 0) {
            velocity = clamp(vx, 3, 5);
          } else if (vx < 0) {
            velocity = clamp(Math.abs(vx), 3, 5) * -1;
          }

          if (Math.abs(dx) > SWIPE_THRESHOLD) {
            Animated.decay(_animation, {
              velocity: { x: velocity, y: vy },
              deceleration: 0.979,
              useNativeDriver: true,
            }).start(transitionNext);
          } else {
            Animated.spring(_animation, {
              toValue: { x: 0, y: 0 },
              friction: 4,
              useNativeDriver: true,
            }).start();
          }
        },
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const rotate = _animation.x.interpolate({
    inputRange: [-200, 0, 200],
    outputRange: ['-30deg', '0deg', '30deg'],
    extrapolate: 'clamp',
  });

  const opacity = _animation.x.interpolate({
    inputRange: [-200, 0, 200],
    outputRange: [0.5, 1, 0.5],
    extrapolate: 'clamp',
  });

  const animatedCardStyles = {
    opacity: _opacity,
    transform: [{ rotate }, ..._animation.getTranslateTransform()],
  };

  const animatedImageStyles = {
    opacity,
  };

  const yesOpacity = _animation.x.interpolate({
    inputRange: [0, 150],
    outputRange: [0, 1],
  });

  const yesScale = _animation.x.interpolate({
    inputRange: [0, 150],
    outputRange: [0.5, 1],
    extrapolate: 'clamp',
  });

  const animatedYesStyles = {
    transform: [{ scale: yesScale }, { rotate: '-30deg' }],
    opacity: yesOpacity,
  };

  const noOpacity = _animation.x.interpolate({
    inputRange: [-150, 0],
    outputRange: [1, 0],
  });

  const noScale = _animation.x.interpolate({
    inputRange: [-150, 0],
    outputRange: [1, 0.5],
    extrapolate: 'clamp',
  });

  const animatedNoStyles = {
    transform: [{ scale: noScale }, { rotate: '30deg' }],
    opacity: noOpacity,
  };

  return (
    <View style={styles.screen}>
      <View style={styles.top}>
        {cats
          .slice(0, 2)
          .reverse()
          .map(({ image, id, text }, index, items) => {
            const isLast = index === items.length - 1;
            const isSecondToLast = index === items.length - 2;

            const pan = isLast ? panResponder.panHandlers : {};
            const cardStyles = isLast ? animatedCardStyles : undefined;
            const imageStyles = isLast ? animatedImageStyles : undefined;

            const nextStyles = isSecondToLast
              ? {
                  transform: [{ scale: _next }],
                }
              : undefined;

            return (
              <Animated.View key={id} style={[styles.card, cardStyles, nextStyles]} {...pan}>
                <Animated.Image
                  source={image}
                  resizeMode="cover"
                  style={[styles.image, imageStyles]}
                />
                <View style={styles.lowerText}>
                  <Text>{text}</Text>
                </View>

                {isLast && (
                  <Animated.View style={[styles.nope, animatedNoStyles]}>
                    <Text style={styles.nopeText}>Nope!</Text>
                  </Animated.View>
                )}

                {isLast && (
                  <Animated.View style={[styles.yup, animatedYesStyles]}>
                    <Text style={styles.yupText}>Yup!</Text>
                  </Animated.View>
                )}
              </Animated.View>
            );
          })}
      </View>
      <View style={styles.buttonBar}>
        <TouchableOpacity style={[styles.button, styles.nopeButton]} onPress={handleNo}>
          <Text>Nope</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, styles.yupButton]} onPress={handleYes}>
          <Text>Yup</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
