import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, Animated, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    animation.current.setValue(0);
    Animated.timing(animation.current, {
      toValue: 1,
      duration: 1500,
      useNativeDriver: false,
    }).start(() => setIsAnimationRunning(false));
  };

  const boxStyle = {
    backgroundColor: animation.current.interpolate({
      inputRange: [0, 1],
      outputRange: ['rgb(255,99,71)', 'rgb(99,71,255)'],
    }),
  };

  const bgStyle = {
    backgroundColor: animation.current.interpolate({
      inputRange: [0, 1],
      outputRange: ['rgba(255,99,71, 0)', 'rgba(255,99,71, 1)'],
    }),
  };

  return (
    <Animated.View style={[styles.screen, bgStyle]}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, boxStyle]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </Animated.View>
  );
};
