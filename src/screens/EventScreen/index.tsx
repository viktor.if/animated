import React, { useRef } from 'react';
import { View, Animated, ViewStyle, Text, ScrollView } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));

  const interpolated = animation.current.interpolate({
    inputRange: [0, 1800],
    outputRange: ['#FF6347', '#00A3A3'],
  });

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    backgroundColor: interpolated,
  };

  return (
    <View style={styles.screen}>
      <ScrollView
        contentContainerStyle={styles.scroll}
        scrollEventThrottle={16} // for js animation
        // scrollEventThrottle={1} // for native animation
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: animation.current,
                },
              },
            },
          ],
          {
            useNativeDriver: false,
          },
        )}
      >
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Scroll me down</Text>
          <Text style={styles.text}>Scroll me up</Text>
        </Animated.View>
      </ScrollView>
    </View>
  );
};
