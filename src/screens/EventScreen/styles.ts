import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../styles';

export default StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  scroll: {
    flexGrow: 1,
    backgroundColor: 'cyan',
    width: Metrics.screenWidth,
  },
  box: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    height: 1800,
  },
  text: {
    color: Colors.white,
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontWeight: 'bold',
    marginVertical: 32,
    textTransform: 'uppercase',
  },
});
