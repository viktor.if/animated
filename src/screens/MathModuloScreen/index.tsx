import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animationValue = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    animationValue.current.setValue(0);
    Animated.timing(animationValue.current, {
      toValue: 360,
      duration: 5000,
      useNativeDriver: true,
    }).start(() => setIsAnimationRunning(false));
  };

  const interpolatedOrigin = animationValue.current.interpolate({
    inputRange: [0, 360],
    outputRange: ['0deg', '360deg'],
  });

  const translationOrigin: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ rotate: interpolatedOrigin }],
  };

  const animationValueModified = Animated.modulo(animationValue.current, 3);

  const interpolatedModified = animationValueModified.interpolate({
    inputRange: [0, 3],
    outputRange: ['0deg', '360deg'],
  });

  const translationModified: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ rotate: interpolatedModified }],
  };

  return (
    <View style={styles.screen}>
      <View style={styles.boxContainer}>
        <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
          <Animated.View style={[styles.box, translationOrigin]}>
            <Text style={styles.text}>Press us</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
          <Animated.View style={[styles.box, translationModified]}>
            <Text style={styles.text}>Press us</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
