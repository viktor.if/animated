import React, { useMemo, useRef } from 'react';
import {
  View,
  Animated,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleProp,
  ViewStyle,
} from 'react-native';
import styles from './styles';

export default () => {
  const animation = useMemo(() => new Animated.Value(0), []);
  const _enabled = useRef<boolean>(true);
  const scrollRef = useRef<ScrollView>(null);

  const handleToggle = () => {
    _enabled.current = !_enabled.current;
    const style: StyleProp<ViewStyle> = [styles.scroll];

    if (!_enabled.current) {
      style.push(styles.hide);
    } else {
      style.push(styles.show);
    }

    scrollRef.current?.setNativeProps({
      scrollEnabled: _enabled.current,
      style,
    });
  };

  const bgInterpalate = animation.interpolate({
    inputRange: [0, 3000],
    outputRange: ['rgb(255,99,71)', 'rgb(99,71,255)'],
  });

  const scrollStyle = {
    backgroundColor: bgInterpalate,
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleToggle}>
        <Text style={styles.text}>Toggle scroll</Text>
      </TouchableOpacity>
      <ScrollView
        ref={scrollRef}
        style={styles.scroll}
        scrollEventThrottle={16}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: animation } } }], {
          useNativeDriver: false,
        })}
      >
        <Animated.View style={[styles.fakeContent, scrollStyle]} />
      </ScrollView>
    </View>
  );
};
