import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    animation.current.setValue(0);
    Animated.timing(animation.current, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: false,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const interpolatedWidth = animation.current.interpolate({
    inputRange: [0, 1],
    outputRange: ['40%', '80%'],
  });

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    width: interpolatedWidth,
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
