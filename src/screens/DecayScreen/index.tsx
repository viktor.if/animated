import React, { useRef, useMemo } from 'react';
import { View, Animated, ViewStyle, Text, PanResponder } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.ValueXY>(new Animated.ValueXY({ x: 0, y: 0 }));

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: animation.current.getTranslateTransform(),
  };

  const _panResponder = useMemo(() => {
    return PanResponder.create({
      onStartShouldSetPanResponderCapture: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event(
        [null, { dx: animation.current.x, dy: animation.current.y }],
        { useNativeDriver: false },
      ),
      onPanResponderRelease: (_, { vx, vy }) => {
        Animated.decay(animation.current, {
          velocity: { x: vx, y: vy },
          deceleration: 0.996,
          useNativeDriver: false,
        }).start();
      },
      onPanResponderGrant: () => animation.current.extractOffset(),
    });
  }, []);

  return (
    <View style={styles.screen}>
      <Animated.View style={[styles.box, animatedStyles]} {..._panResponder.panHandlers}>
        <Text style={styles.text}>Drag me</Text>
      </Animated.View>
    </View>
  );
};
