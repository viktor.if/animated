import { StyleSheet } from 'react-native';
import { Colors } from '../../styles';

export default StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  box: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    width: 60,
    backgroundColor: 'tomato',
  },
  text: {
    fontSize: 10,
    color: Colors.white,
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
});
