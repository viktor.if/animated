import React, { useMemo, useState } from 'react';
import { View, Animated, TouchableWithoutFeedback, RegisteredStyle, ViewStyle } from 'react-native';
import Heart from './Heart';

import styles from './styles';

const getTransformationAnimation = (
  animation: Animated.Value,
  scale: number,
  y: number,
  x: number,
  rotate: string,
  opacity: number,
):
  | false
  | Animated.Value
  | Animated.AnimatedInterpolation
  | RegisteredStyle<ViewStyle>
  | Animated.WithAnimatedObject<ViewStyle> => {
  const scaleAnimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, scale],
  });

  const xAnimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, x],
  });

  const yAnimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, y],
  });

  const rotateAnimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', rotate],
  });

  const opacityAnimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, opacity],
  });

  return {
    opacity: opacityAnimation,
    transform: [
      {
        scale: scaleAnimation,
      },
      {
        translateX: xAnimation,
      },
      {
        translateY: yAnimation,
      },
      {
        rotate: rotateAnimation,
      },
    ],
  };
};

export default () => {
  const [liked, setLiked] = useState<boolean>(false);
  const scale = useMemo(() => new Animated.Value(0), []);
  const [animations] = useState<Animated.Value[]>([
    new Animated.Value(0),
    new Animated.Value(0),
    new Animated.Value(0),
    new Animated.Value(0),
    new Animated.Value(0),
    new Animated.Value(0),
  ]);

  const triggerLike = () => {
    setLiked((prev) => !prev);

    const showAnimations = animations.map((animation) => {
      return Animated.spring(animation, { toValue: 1, friction: 4, useNativeDriver: false });
    });

    const hideAnimations = animations
      .map((animation) => {
        return Animated.timing(animation, { toValue: 0, duration: 50, useNativeDriver: false });
      })
      .reverse();

    Animated.parallel([
      Animated.spring(scale, { toValue: 2, friction: 3, useNativeDriver: false }),
      Animated.sequence([
        Animated.stagger(50, showAnimations),
        Animated.delay(100),
        Animated.stagger(50, hideAnimations),
      ]),
    ]).start(() => {
      scale.setValue(0);
    });
  };

  const bouncyHeart = scale.interpolate({
    inputRange: [0, 1, 2],
    outputRange: [1, 0.8, 1],
  });

  const heartButtonStyle = {
    transform: [{ scale: bouncyHeart }],
  };

  return (
    <View style={styles.container}>
      <View>
        {/** 1 */}
        <Heart
          filled
          style={[
            styles.heart,
            getTransformationAnimation(animations[5], 0.4, -280, 0, '10deg', 0.7),
          ]}
        />
        {/** 2 */}
        <Heart
          filled
          style={[
            styles.heart,
            getTransformationAnimation(animations[4], 0.7, -120, 40, '45deg', 0.5),
          ]}
        />
        {/** 3 */}
        <Heart
          filled
          style={[
            styles.heart,
            getTransformationAnimation(animations[3], 0.8, -120, -40, '-45deg', 0.3),
          ]}
        />
        {/** 4 */}
        <Heart
          filled
          style={[
            styles.heart,
            getTransformationAnimation(animations[2], 0.3, -150, 120, '-35deg', 0.6),
          ]}
        />
        {/** 5 */}
        <Heart
          filled
          style={[
            styles.heart,
            getTransformationAnimation(animations[1], 0.3, -120, -120, '-35deg', 0.7),
          ]}
        />
        {/** 6 */}
        <Heart
          filled
          style={[
            styles.heart,
            getTransformationAnimation(animations[0], 0.8, -60, 0, '35deg', 0.8),
          ]}
        />
        <TouchableWithoutFeedback onPress={triggerLike}>
          <Animated.View style={heartButtonStyle}>
            <Heart filled={liked} />
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
