import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text } from 'react-native';
import styles from './styles';

export default () => {
  const scaleAnimation = useRef<Animated.Value>(new Animated.Value(1));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    scaleAnimation.current.setValue(1);
    Animated.timing(scaleAnimation.current, {
      toValue: 3,
      duration: 2000,
      useNativeDriver: true,
    }).start(() => setIsAnimationRunning(false));
  };

  const scaleInterpolated = scaleAnimation.current.interpolate({
    inputRange: [1, 2],
    outputRange: [1, 2],
    // extrapolate: 'extend', // default
    extrapolate: 'clamp',
    // extrapolate: 'identity',
  });

  const styleBox: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ scale: scaleInterpolated }],
  };

  return (
    <View style={styles.screen}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, styleBox]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
