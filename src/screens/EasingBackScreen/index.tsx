import React, { useRef, useState } from 'react';
import { TouchableWithoutFeedback, View, Animated, ViewStyle, Text, LayoutChangeEvent, Easing } from 'react-native';
import styles from './styles';

export default () => {
  const animation = useRef<Animated.Value>(new Animated.Value(0));
  const [isAnimationRunning, setIsAnimationRunning] = useState<boolean>(false);
  const [containerHeight, setContainerHeight] = useState<number>(0);

  const startAnimation = () => {
    setIsAnimationRunning(true);
    animation.current.setValue(0);
    Animated.timing(animation.current, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: false,
      easing: Easing.back(5),
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start(() => setIsAnimationRunning(false));
    });
  };

  const onLayout = (event: LayoutChangeEvent): void => {
    setContainerHeight(event.nativeEvent.layout.height);
  };

  const interpolatedTranslationY = animation.current.interpolate({
    inputRange: [0, 1],
    outputRange: [0, (containerHeight - 160) / 2],
  });

  const animatedStyles: Animated.WithAnimatedObject<ViewStyle> = {
    transform: [{ translateY: interpolatedTranslationY }],
  };

  return (
    <View style={styles.screen} onLayout={onLayout}>
      <TouchableWithoutFeedback onPress={startAnimation} disabled={isAnimationRunning}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text style={styles.text}>Press me</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};
