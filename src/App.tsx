import React from 'react';
import 'react-native-gesture-handler';
import NavigationContainer from './navigators/NavigationContainer';

export default () => <NavigationContainer />;
