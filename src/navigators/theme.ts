import { DefaultTheme, Theme } from '@react-navigation/native';
import { Colors } from '../styles';

export const theme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.black,
    background: Colors.white,
    text: Colors.black,
  },
};
