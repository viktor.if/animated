import React from 'react';
import { StackNavigationOptions, TransitionPresets } from '@react-navigation/stack';
import ChevronLeftIcon from '../../assets/svg/chevron-left.svg';
import { Colors } from '../styles';
import { Platform } from 'react-native';

export const defaultOptions: Pick<
  StackNavigationOptions,
  | 'headerStyle'
  | 'headerTintColor'
  | 'headerTitleStyle'
  | 'headerTitleAlign'
  | 'headerBackTitleVisible'
  | 'headerBackImage'
  | 'gestureEnabled'
> = {
  ...TransitionPresets.SlideFromRightIOS,
  headerBackTitleVisible: false,
  headerStyle: {
    backgroundColor: Colors.greyish1,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.greyish2,
  },
  headerTintColor: 'black',
  gestureEnabled: true,
  headerTitleStyle: {
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontFamily: 'System',
    fontSize: 17,
    lineHeight: 22,
    letterSpacing: -0.41,
  },
  headerTitleAlign: 'center',
  headerBackImage: ({ tintColor }) => (
    <ChevronLeftIcon
      width={12}
      height={22}
      fill={tintColor}
      style={{ marginLeft: Platform.OS === 'ios' ? 12 : undefined }}
    />
  ),
};
