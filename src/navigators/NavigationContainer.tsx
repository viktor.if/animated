import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { theme } from './theme';
import { defaultOptions } from './options';
import { RootStackParamList } from './types';
import { Pages } from './Routes';
import useNavigationPersistence from './useNavigationPersistence';

/** Screens */
import OpacityScreen from '../screens/OpacityScreen';
import HomeScreen from '../screens/HomeScreen';
import TranslationScreen from '../screens/TranslationScreen';
import ScaleScreen from '../screens/ScaleScreen';
import ScaleXYScreen from '../screens/ScaleXYScreen';
import ScaleReverseScreen from '../screens/ScaleReverseScreen';
import HeightValueScreen from '../screens/HeightValueScreen';
import AbsolutePositionScreen from '../screens/AbsolutePositionScreen';
import ColorScreen from '../screens/ColorScreen';
import RotationScreen from '../screens/RotationScreen';
import RotationYScreen from '../screens/RotationYScreen';
import WidhtHeightPercentageScreen from '../screens/WidhtHeightPercentageScreen';
import EasingBackScreen from '../screens/EasingBackScreen';
import EasingBounceScreen from '../screens/EasingBounceScreen';
import EasingElasticScreen from '../screens/EasingElasticScreen';
import EasingBezierScreen from '../screens/EasingBezierScreen';
import SpringScreen from '../screens/SpringScreen';
import LoopScreen from '../screens/LoopScreen';
import EventScreen from '../screens/EventScreen';
import DecayScreen from '../screens/DecayScreen';
import MathAddScreen from '../screens/MathAddScreen';
import MathDivideScreen from '../screens/MathDivideScreen';
import MathMultiplyScreen from '../screens/MathMultiplyScreen';
import MathModuloScreen from '../screens/MathModuloScreen';
import ParallelAnimationsScreen from '../screens/ParallelAnimationsScreen';
import SequenceAnimationsScreen from '../screens/SequenceAnimationsScreen';
import StaggerScreen from '../screens/StaggerScreen ';
import DelayScreen from '../screens/DelayScreen';
import NumberInterpolationScreen from '../screens/NumberInterpolationScreen';
import ColorInterpolationScreen from '../screens/ColorInterpolationScreen';
import ExtrapolateScreen from '../screens/ExtrapolateScreen';
import GesturesAndAnimationsScreen from '../screens/GesturesAndAnimationsScreen';
import CreateAnimatedComponentScreen from '../screens/CreateAnimatedComponentScreen';
import SetNativePropsScreen from '../screens/SetNativePropsScreen';
import D3InterpolateScreen from '../screens/Dot99Cliff';
import D3InterpolatePathScreen from '../screens/D3InterpolatePathScreen';
import ArtAndTweenScreen from '../screens/ArtAndTweenScreen';
import FlubberScreen from '../screens/FlubberScreen';
import Dot99Cliff from '../screens/Dot99Cliff';
import HiddenScreen from '../screens/HiddenScreen';
import InterruptionScreen from '../screens/InterruptionScreen';
import PointerEventsScreen from '../screens/PointerEventsScreen';
import _4CornersScreen from '../screens/_4CornersScreen';
import StaggeredHeadsScreen from '../screens/StaggeredHeadsScreen';
import KittenCardsScreen from '../screens/KittenCardsScreen';
import StaggerOnMountScreen from '../screens/StaggerOnMountScreen';
import ProgressBarButtonScreen from '../screens/ProgressBarButtonScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import QuestionnaireScreen from '../screens/QuestionnaireScreen';
import PhotoGridScreen from '../screens/PhotoGridScreen';
import ColorPickerScreen from '../screens/ColorPickerScreen';
import FloatingActionButtonScreen from '../screens/FloatingActionButtonScreen';
import IntroScreenScreen from '../screens/IntroScreenScreen';
import EvolvingButtonScreen from '../screens/EvolvingButtonScreen';
import SocialCommentModalScreen from '../screens/SocialCommentModalScreen';
import HorizontalParallaxScreen from '../screens/HorizontalParallaxScreen';
import FloatingHeartsScreen from '../screens/FloatingHeartsScreen';
import BouncingHeartScreen from '../screens/BouncingHeartScreen';
import ExploadingHeartScreen from '../screens/ExploadingHeartScreen';
import ExpandingNotifyInputScreen from '../screens/ExpandingNotifyInputScreen';

const Stack = createStackNavigator<RootStackParamList>();

export default () => {
  const { onStateChange, initialState, isReady } = useNavigationPersistence();

  if (!isReady) {
    return null;
  }

  return (
    <NavigationContainer {...{ theme, onStateChange, initialState }}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />
      <Stack.Navigator screenOptions={defaultOptions}>
        <Stack.Screen
          name={Pages.HomeScreen}
          component={HomeScreen}
          options={{ title: 'Animated' }}
        />
        <Stack.Screen
          name={Pages.OpacityScreen}
          component={OpacityScreen}
          options={{ title: 'Opacity' }}
        />
        <Stack.Screen
          name={Pages.TranslationScreen}
          component={TranslationScreen}
          options={{ title: 'Translation' }}
        />
        <Stack.Screen
          name={Pages.ScaleScreen}
          component={ScaleScreen}
          options={{ title: 'Scale' }}
        />
        <Stack.Screen
          name={Pages.ScaleXYScreen}
          component={ScaleXYScreen}
          options={{ title: 'ScaleXY' }}
        />
        <Stack.Screen
          name={Pages.ScaleReverseScreen}
          component={ScaleReverseScreen}
          options={{ title: 'Scale Reverse' }}
        />
        <Stack.Screen
          name={Pages.HeightValueScreen}
          component={HeightValueScreen}
          options={{ title: 'Height Value' }}
        />
        <Stack.Screen
          name={Pages.AbsolutePositionScreen}
          component={AbsolutePositionScreen}
          options={{ title: 'Absolute Position' }}
        />
        <Stack.Screen
          name={Pages.ColorScreen}
          component={ColorScreen}
          options={{ title: 'Color' }}
        />
        <Stack.Screen
          name={Pages.RotationScreen}
          component={RotationScreen}
          options={{ title: 'Rotation' }}
        />
        <Stack.Screen
          name={Pages.RotationYScreen}
          component={RotationYScreen}
          options={{ title: 'RotationXY' }}
        />
        <Stack.Screen
          name={Pages.WidhtHeightPercentageScreen}
          component={WidhtHeightPercentageScreen}
          options={{ title: 'Width/Height Percentage' }}
        />
        <Stack.Screen
          name={Pages.EasingBackScreen}
          component={EasingBackScreen}
          options={{ title: 'Easing Back' }}
        />
        <Stack.Screen
          name={Pages.EasingBounceScreen}
          component={EasingBounceScreen}
          options={{ title: 'Easing Bounce' }}
        />
        <Stack.Screen
          name={Pages.EasingElasticScreen}
          component={EasingElasticScreen}
          options={{ title: 'Easing Elastic' }}
        />
        <Stack.Screen
          name={Pages.EasingBezierScreen}
          component={EasingBezierScreen}
          options={{ title: 'Easing Bezier' }}
        />
        <Stack.Screen
          name={Pages.SpringScreen}
          component={SpringScreen}
          options={{ title: 'Spring' }}
        />
        <Stack.Screen name={Pages.LoopScreen} component={LoopScreen} options={{ title: 'Loop' }} />
        <Stack.Screen
          name={Pages.EventScreen}
          component={EventScreen}
          options={{ title: 'Event' }}
        />
        <Stack.Screen
          name={Pages.DecayScreen}
          component={DecayScreen}
          options={{ title: 'Decay' }}
        />
        <Stack.Screen
          name={Pages.MathAddScreen}
          component={MathAddScreen}
          options={{ title: 'Math Add' }}
        />
        <Stack.Screen
          name={Pages.MathDivideScreen}
          component={MathDivideScreen}
          options={{ title: 'Math Divide' }}
        />
        <Stack.Screen
          name={Pages.MathMultiplyScreen}
          component={MathMultiplyScreen}
          options={{ title: 'Math Multiply' }}
        />
        <Stack.Screen
          name={Pages.MathModuloScreen}
          component={MathModuloScreen}
          options={{ title: 'Math Modulo' }}
        />
        <Stack.Screen
          name={Pages.ParallelAnimationsScreen}
          component={ParallelAnimationsScreen}
          options={{ title: 'Parallel' }}
        />
        <Stack.Screen
          name={Pages.SequenceAnimationsScreen}
          component={SequenceAnimationsScreen}
          options={{ title: 'Sequence' }}
        />
        <Stack.Screen
          name={Pages.StaggerScreen}
          component={StaggerScreen}
          options={{ title: 'Stagger' }}
        />
        <Stack.Screen
          name={Pages.DelayScreen}
          component={DelayScreen}
          options={{ title: 'Delay' }}
        />
        <Stack.Screen
          name={Pages.NumberInterpolationScreen}
          component={NumberInterpolationScreen}
          options={{ title: 'Number Interpolation' }}
        />
        <Stack.Screen
          name={Pages.ColorInterpolationScreen}
          component={ColorInterpolationScreen}
          options={{ title: 'Color Interpolation' }}
        />
        <Stack.Screen
          name={Pages.ExtrapolateScreen}
          component={ExtrapolateScreen}
          options={{ title: 'Extrapolation' }}
        />
        <Stack.Screen
          name={Pages.GesturesAndAnimationsScreen}
          component={GesturesAndAnimationsScreen}
          options={{ title: 'Gestures & Animations' }}
        />
        <Stack.Screen
          name={Pages.CreateAnimatedComponentScreen}
          component={CreateAnimatedComponentScreen}
          options={{ title: 'Create Animated Component' }}
        />
        <Stack.Screen
          name={Pages.SetNativePropsScreen}
          component={SetNativePropsScreen}
          options={{ title: 'Set Native Props' }}
        />
        <Stack.Screen
          name={Pages.D3InterpolateScreen}
          component={D3InterpolateScreen}
          options={{ title: 'D3 Interpolate' }}
        />
        <Stack.Screen
          name={Pages.D3InterpolatePathScreen}
          component={D3InterpolatePathScreen}
          options={{ title: 'D3 Interpolate Path' }}
        />
        <Stack.Screen
          name={Pages.ArtAndTweenScreen}
          component={ArtAndTweenScreen}
          options={{ title: 'Art and Morph.Tween' }}
        />
        <Stack.Screen
          name={Pages.FlubberScreen}
          component={FlubberScreen}
          options={{ title: 'Flubber' }}
        />
        <Stack.Screen
          name={Pages.Dot99Cliff}
          component={Dot99Cliff}
          options={{ title: '.99 Cliff' }}
        />
        <Stack.Screen
          name={Pages.HiddenScreen}
          component={HiddenScreen}
          options={{ title: 'Hidden' }}
        />
        <Stack.Screen
          name={Pages.InterruptionScreen}
          component={InterruptionScreen}
          options={{ title: 'Interruption' }}
        />
        <Stack.Screen
          name={Pages.PointerEventsScreen}
          component={PointerEventsScreen}
          options={{ title: 'Pointer Events' }}
        />
        <Stack.Screen
          name={Pages._4CornersScreen}
          component={_4CornersScreen}
          options={{ title: '4 Corners' }}
        />
        <Stack.Screen
          name={Pages.StaggeredHeadsScreen}
          component={StaggeredHeadsScreen}
          options={{ title: 'Staggered Heads' }}
        />
        <Stack.Screen
          name={Pages.KittenCardsScreen}
          component={KittenCardsScreen}
          options={{ title: 'Kitten Cards' }}
        />
        <Stack.Screen
          name={Pages.StaggerOnMountScreen}
          component={StaggerOnMountScreen}
          options={{ title: 'Stagger on Mount' }}
        />
        <Stack.Screen
          name={Pages.ProgressBarButtonScreen}
          component={ProgressBarButtonScreen}
          options={{ title: 'Progress Bar Button' }}
        />
        <Stack.Screen
          name={Pages.NotificationsScreen}
          component={NotificationsScreen}
          options={{ title: 'Notifications' }}
        />
        <Stack.Screen
          name={Pages.QuestionnaireScreen}
          component={QuestionnaireScreen}
          options={{ title: 'Questionnaire' }}
        />
        <Stack.Screen
          name={Pages.PhotoGridScreen}
          component={PhotoGridScreen}
          options={{ title: 'Photo Grid' }}
        />
        <Stack.Screen
          name={Pages.ColorPickerScreen}
          component={ColorPickerScreen}
          options={{ title: 'Color Picker' }}
        />
        <Stack.Screen
          name={Pages.FloatingActionButtonScreen}
          component={FloatingActionButtonScreen}
          options={{ title: 'Floating Action Button' }}
        />
        <Stack.Screen
          name={Pages.IntroScreenScreen}
          component={IntroScreenScreen}
          options={{ title: 'Intro Screen' }}
        />
        <Stack.Screen
          name={Pages.EvolvingButtonScreen}
          component={EvolvingButtonScreen}
          options={{ title: 'Evolving Button' }}
        />
        <Stack.Screen
          name={Pages.SocialCommentModalScreen}
          component={SocialCommentModalScreen}
          options={{ title: 'Social Comment Modal' }}
        />
        <Stack.Screen
          name={Pages.HorizontalParallaxScreen}
          component={HorizontalParallaxScreen}
          options={{ title: 'Horizontal Parallax' }}
        />
        <Stack.Screen
          name={Pages.FloatingHeartsScreen}
          component={FloatingHeartsScreen}
          options={{ title: 'Love Floating Hearts' }}
        />
        <Stack.Screen
          name={Pages.BouncingHeartScreen}
          component={BouncingHeartScreen}
          options={{ title: 'Bouncing Heart' }}
        />
        <Stack.Screen
          name={Pages.ExploadingHeartScreen}
          component={ExploadingHeartScreen}
          options={{ title: 'Exploading Heart' }}
        />
        <Stack.Screen
          name={Pages.ExpandingNotifyInputScreen}
          component={ExpandingNotifyInputScreen}
          options={{ title: 'Expanding Notify Input' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
